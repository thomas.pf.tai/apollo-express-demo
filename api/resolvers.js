import { find, filter } from 'lodash';
import {FortuneCookie } from './connectors';

const defaultData = [
	{
		id: 1,
		name: 'Luke SkyWalker',
		gender: 'male',
		homeworld: 'Tattoine'
	},
	{
		id: 2,
		name: 'C-3PO',
		gender: 'bot',
		homeworld: 'Tattoine'
	},
	{
		id: 3,
		name: 'Darth Vader',
		gender: 'male',
		homeworld: 'Tattoine'
	}
];

const resolvers = {
	Query: {
		info: () => { return 'This is my first try on GraphQL'},
		allPeople: () => {
			return defaultData;
		},
		person: (root, args, context, info) => {
      		return find(defaultData, { id: args.id });
    	},
  // 		person: (root, { id }) => {
		// 	return defaultData.filter(character => {
		// 		return (character.id == id);
		// 	})[0];
		// }
    	peopleByGender: (root, args, context, info) => {
    		return filter(defaultData, { gender: args.gender });
    	},
    	getFortuneCookie: () => {
    		return FortuneCookie.getOne();
    	}
	}
};

export default resolvers;