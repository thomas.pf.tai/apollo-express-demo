import { makeExecutableSchema } from 'graphql-tools';

import resolvers from './resolvers';


const typeDefs = `
	type Person {
		id: Int
		name: String
		gender: String
		homeworld: String
	}
	type Query {
		info: String!
		allPeople: [Person]
		person(id: Int!): Person
		peopleByGender(gender: String): [Person]
		getFortuneCookie: String
	}
`;

const schema = makeExecutableSchema( { typeDefs, resolvers } );

export default typeDefs;